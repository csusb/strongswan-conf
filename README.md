# Setup


Add various `*.pub` certificates to `/etc/ipsec.d/cacerts`

  * confirm via `sudo ipsec rereadcacerts; sudo ipsec listcacerts`

Copy *your* `*.p12` to  `/etc/ipsec.d/private`

  * confirm via `sudo ipsec secrets; sudo ipsec listcerts` making sure the _has private key_ is listed

Copy `ipsec.conf` to /etc/ipsec.conf

Copy `ipsec.csusb-usernames.conf.example` to /etc/ipsec.csusb-usernames.conf

  * modify `xauth_identity`
  * modify `leftid`  to match one of the subjectAltName of your p12 certificate

Copy `ipsec.secrets.example` to `/etc/ipsec.secrets`

   * chmod 600
   * modify p12 filename and xauth username

# Connecting

  1. `sudo ipsec secrets` to read in your p12 file
  1. `sudo ipsec stroke user-creds csusb 001234567` to read in your XAuth password.  
      This only stays valid for a few minutes
  1. `sudo ipsec up csusb`
  1. Once connected, hack do split tunnel routing
    1. (this should really be done using the updown script)
    1. `sudo ip route show table 220` take a look at what the default updown put there
    1. `sudo ip route add 139.182.0.0/16 via <same.as.default> src <same.as.default> table 220`
    1. `sudo ip route add 10.120.0.0/16 via <same.as.default> src <same.as.default> table 220`
    1. `sudo ip route del default table 220`

Things to look for in the output:

```
authentication of 'joe.coyote@csusb.edu' (myself) successful
[...]
authentication of 'C=US, [...blah blah...] CN=secure.csusb.edu' with RSA successful
[...]
XAuth authentication of '001234567' (myself) successful
```
